<?php
require_once 'vendor/autoload.php';
require_once 'src/Letters/LettersGenerator.php';
require_once 'src/Letters/LettersParser.php';
require_once 'src/Dimensions/DimensionScanner.php';
require_once 'src/QuestionHandler.php';
require_once 'src/SkillHandler.php';
require_once 'src/RecommendationHandler.php';
require_once 'src/MetaHandler.php';
require_once 'src/DataBuilder.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use src\DataBuilder;
use src\Dimensions\DimensionScanner;
use src\MetaHandler;
use src\QuestionHandler;
use src\RecommendationHandler;
use src\SkillHandler;

$spreadsheet = IOFactory::load("input/template.xlsx");

$worksheetQuestions = $spreadsheet->getSheetByName('questions');
$dimQuestions = DimensionScanner::scanDimensions($worksheetQuestions);
$questionHandler = new QuestionHandler($worksheetQuestions, $dimQuestions);

$worksheetSkills = $spreadsheet->getSheetByName('skills');
$dimSkills = DimensionScanner::scanDimensions($worksheetSkills);
$skillHandler = new SkillHandler($worksheetSkills, $dimSkills);

$worksheetMeta = $spreadsheet->getSheetByName('meta');
$dimMeta = DimensionScanner::scanDimensions($worksheetSkills);
$metaHandler = new MetaHandler($worksheetMeta, $dimMeta);

$worksheetRecommandations = $spreadsheet->getSheetByName('questions');
$dimRecommandations = DimensionScanner::scanDimensions($worksheetRecommandations);
$recommandationsHandler = new RecommendationHandler($worksheetRecommandations, $dimRecommandations);

$dataBuilder = new DataBuilder($questionHandler, $skillHandler, $recommandationsHandler, $metaHandler);
?>
