<?php

namespace src;

use src\Letters\LettersGenerator;
use PhpOffice\PhpSpreadsheet\RichText\RichText;

class DataBuilder
{
    private const FILE_PATH_TO_DATA = "../src/data/";


    public function __construct(QuestionHandler $questionHandler, SkillHandler $skillHandler, RecommendationHandler $recommandationHandler, MetaHandler $metaHandler)
    {
        $this->clearFiles();
        $this->createDataQuestions($questionHandler);
        $this->createSkills($skillHandler);
        $this->createRecommandations($recommandationHandler);
        $this->createMeta($metaHandler);
    }

    private function clearFiles()
    {
        if (!file_exists(self::FILE_PATH_TO_DATA . "pictures")) {
            mkdir(self::FILE_PATH_TO_DATA . "pictures");
        }
        foreach (glob(self::FILE_PATH_TO_DATA . "/*") as $filename) {
            if (is_dir($filename)) {
                continue;
            }
            unlink($filename);
        }
        foreach (glob(self::FILE_PATH_TO_DATA . "pictures/*") as $filename) {
            unlink($filename);
        }
    }

    private function createDataQuestions(QuestionHandler $questionHandler)
    {
        $questionDataArr = [];
        $imgMap = [];
        $imgFileNameMap = [];
        $imgDataArr = [];
        foreach ($questionHandler->getDatasets() as $questionDataset) {
            if (isset($imgFileNameMap[$questionDataset['img']])) {
                $imgMapKey = $imgFileNameMap[$questionDataset['img']];
            } else {
                $imgMapKey = LettersGenerator::randomLetters(100);
                $imgMap[$imgMapKey] = $questionDataset['img'];
                $imgFileNameMap[$questionDataset['img']] = $imgMapKey;
                $imgDataArr[] = "import $imgMapKey from './pictures/" . $questionDataset['img'] . "'";
                copy("input/img/" . $questionDataset['img'], self::FILE_PATH_TO_DATA . "pictures/" . $questionDataset['img']);
            }
            $questionDataArr[] = [
                'question' => $this->toString($questionDataset['question']),
                'technology' => $this->toString($questionDataset['technology']),
                'key' => $this->toString($questionDataset['key']),
                'img' => $this->toString($imgMapKey)
            ];
        }

        file_put_contents(self::FILE_PATH_TO_DATA . "QuestionData.tsx", implode("\n", $imgDataArr) . "

export interface QuestionDataType {
    question: string,
    technology: string,
    key: string,
    img: string,
}

const QuestionsData: QuestionDataType[] = [" . $this->renderQuestionData($questionDataArr) . "];

export default QuestionsData;");
    }

    private function renderQuestionData(array $questionDataArr): string
    {
        $arr = [];
        foreach ($questionDataArr as $questionData) {
            $arr[] = "{
                'question': '" . $this->toString($questionData['question']) . "',
                'technology': '" . $this->toString($questionData['technology']) . "',
                'key': '" . $this->toString($questionData['key']) . "',
                'img': " . $this->toString($questionData['img']) . "
            }";
        }
        return implode(',', $arr);
    }

    private function toString($anything) {
        return $anything instanceof RichText ? $anything->getPlainText() : $anything;
    }

    private function createSkills(SkillHandler $skillHandler)
    {
        $skillDataArr = [];
        $i=0;
        foreach ($skillHandler->getDatasets() as $skillDataset) {
            $skillDataArr[] = [
                'key' => $this->toString($skillDataset['key']),
                'skill' => $this->toString($skillDataset['skill']),
                'description' => $this->toString($skillDataset['description']),
                'l1' => $this->toString($skillDataset['l1']),
                'l2' => $this->toString($skillDataset['l2']),
                'l3' => $this->toString($skillDataset['l3']),
                'l4' => $this->toString($skillDataset['l4'])
            ];
            $i++;
        }

        file_put_contents(self::FILE_PATH_TO_DATA . "SkillData.tsx", "export interface SkillDataType {
    key: string,
    skill: string,
    description: string,
    l1: string,
    l2: string,
    l3: string,
    l4: string,
}

const SkillData: SkillDataType[] = " . json_encode($skillDataArr) . ";

export default SkillData;");
    }

    private function createRecommandations(RecommendationHandler $recommendationHandler)
    {
        $activeArr = $recommendationHandler->getActive();
        $passiveArr = $recommendationHandler->getPassive();
        file_put_contents(self::FILE_PATH_TO_DATA . "RecommendationData.tsx", "export interface RecommendationDataType {
    active: string[],
    passive: string[]
}

const RecommendationData: RecommendationDataType = {
    active: " . json_encode($activeArr) . ",
    passive: " . json_encode($passiveArr) . "
};

export default RecommendationData;");
    }

    private function createMeta(MetaHandler $metaHandler)
    {
        $tmp = $metaHandler->getDataset();
        $metaDataset = [];
        $metaDataset[] = [
            'heading' => $tmp['l1_heading'],
            'text' => $tmp['l1_text']
        ];
        $metaDataset[] = [
            'heading' => $tmp['l2_heading'],
            'text' => $tmp['l2_text']
        ];
        $metaDataset[] = [
            'heading' => $tmp['l3_heading'],
            'text' => $tmp['l3_text']
        ];
        $metaDataset[] = [
            'heading' => $tmp['l4_heading'],
            'text' => $tmp['l4_text']
        ];

        file_put_contents(self::FILE_PATH_TO_DATA . "Meta.tsx", "export interface LevelDataType {
    heading: string,
    text: string,
}

const LevelData: LevelDataType[] = " . json_encode($metaDataset) . ";

export default LevelData;");
    }
}