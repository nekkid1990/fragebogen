<?php

namespace src\Dimensions;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use src\Letters\LettersParser;

class DimensionScanner
{
    public static function scanDimensions(Worksheet $worksheet): array
    {
        $output = [
            'x' => null,
            'y' => null
        ];

        $xStartIndex = 1;
        $x = $xStartIndex;
        while ($worksheet->getCell(LettersParser::parseNumberToLetters($x) . "1")->getValue() !== null) {
            $x++;
        }
        $output['x'] = $x - $xStartIndex;

        $yStartIndex = 2;
        $y = $yStartIndex;
        while ($worksheet->getCell("A" . ($y))->getValue() !== null) {
            $y++;
        }
        $output['y'] = $y - $yStartIndex;
        return $output;
    }
}