<?php

namespace src\Letters;

class LettersGenerator
{
    public static function randomLetters(int $length = 0): string
    {
        $randomStr = "";
        while ($length > 0) {
            if (rand(0, 1) === 0) {
                $randomStr .= chr(rand(65, 90));
            } else {
                $randomStr .= chr(rand(97, 122));
            }
            $length--;
        }
        return $randomStr;
    }
}