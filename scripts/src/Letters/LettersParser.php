<?php

namespace src\Letters;

class LettersParser
{
    public static function parseNumberToLetters(int $number): string
    {
        $output = "";
        while ($number > 26) {
            $result = intval($number / 26);
            $remainder = intval($number % 26);
            $output .= chr(64 + $remainder);
            $number = $result;
        }
        if ($number > 0) {
            $output .= chr(64 + $number);
        }
        return strrev($output);
    }
}