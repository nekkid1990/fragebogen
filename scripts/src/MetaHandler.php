<?php

namespace src;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use src\Letters\LettersParser;

class MetaHandler
{
    private array $dataset = [];

    public function __construct(Worksheet $worksheet, array $dimensions)
    {
        for ($y = 2; $y < 3; $y++) {
            $this->dataset = [
                'l1_heading' => $worksheet->getCell(LettersParser::parseNumberToLetters(1) . $y)->getValue(),
                'l1_text' => $worksheet->getCell(LettersParser::parseNumberToLetters(2) . $y)->getValue(),
                'l2_heading' => $worksheet->getCell(LettersParser::parseNumberToLetters(3) . $y)->getValue(),
                'l2_text' => $worksheet->getCell(LettersParser::parseNumberToLetters(4) . $y)->getValue(),
                'l3_heading' => $worksheet->getCell(LettersParser::parseNumberToLetters(5) . $y)->getValue(),
                'l3_text' => $worksheet->getCell(LettersParser::parseNumberToLetters(6) . $y)->getValue(),
                'l4_heading' => $worksheet->getCell(LettersParser::parseNumberToLetters(7) . $y)->getValue(),
                'l4_text' => $worksheet->getCell(LettersParser::parseNumberToLetters(8) . $y)->getValue()
            ];
        }
    }

    public function getDataset(): array
    {
        return $this->dataset;
    }
}