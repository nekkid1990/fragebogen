<?php

namespace src;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use src\Letters\LettersParser;

class QuestionHandler
{
    private array $datasets = [];

    public function __construct(Worksheet $worksheet, array $dimensions)
    {
        for ($y = 2; $y < $dimensions['y'] + 2; $y++) {
            $this->datasets[] = [
                'question' => $worksheet->getCell(LettersParser::parseNumberToLetters(1) . $y)->getValue(),
                'technology' => $worksheet->getCell(LettersParser::parseNumberToLetters(2) . $y)->getValue(),
                'key' => $worksheet->getCell(LettersParser::parseNumberToLetters(3) . $y)->getValue(),
                'img' => $worksheet->getCell(LettersParser::parseNumberToLetters(4) . $y)->getValue()
            ];
        }
    }

    public function getDatasets(): array
    {
        return $this->datasets;
    }
}