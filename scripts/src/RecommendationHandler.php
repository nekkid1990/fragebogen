<?php

namespace src;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use src\Letters\LettersParser;

class RecommendationHandler
{
    private array $actives = [];
    private array $passives = [];

    public function __construct(Worksheet $worksheet, array $dimensions)
    {
        for ($y = 2; $y < $dimensions['y'] + 2; $y++) {
            $this->actives[] = $worksheet->getCell(LettersParser::parseNumberToLetters(5) . $y)->getValue();
            $this->passives[] = $worksheet->getCell(LettersParser::parseNumberToLetters(6) . $y)->getValue();
        }
    }

    public function getActive(): array
    {
        return $this->actives;
    }

    public function getPassive(): array
    {
        return $this->passives;
    }
}