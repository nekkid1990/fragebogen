<?php

namespace src;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use src\Letters\LettersParser;

class SkillHandler
{
    private array $datasets = [];

    public function __construct(Worksheet $worksheet, array $dimensions)
    {
        for ($y = 2; $y < $dimensions['y'] + 2; $y++) {
            $this->datasets[] = [
                'key' => $worksheet->getCell(LettersParser::parseNumberToLetters(1) . $y)->getValue(),
                'skill' => $worksheet->getCell(LettersParser::parseNumberToLetters(2) . $y)->getValue(),
                'description' => $worksheet->getCell(LettersParser::parseNumberToLetters(3) . $y)->getValue(),
                'l1' => $worksheet->getCell(LettersParser::parseNumberToLetters(4) . $y)->getValue(),
                'l2' => $worksheet->getCell(LettersParser::parseNumberToLetters(5) . $y)->getValue(),
                'l3' => $worksheet->getCell(LettersParser::parseNumberToLetters(6) . $y)->getValue(),
                'l4' => $worksheet->getCell(LettersParser::parseNumberToLetters(7) . $y)->getValue()
            ];
        }
    }

    public function getDatasets(): array
    {
        return $this->datasets;
    }
}