import React from "react";
import "./App.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Questionnaire from "./pages/Questionnaire";
import Results from "./pages/Results";
import Splashscreen from "./pages/Splashscreen";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Splashscreen />
          </Route>
          <Route path="/questionnaire">
            <Questionnaire />
          </Route>
          <Route path="/result">
            <Results />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
