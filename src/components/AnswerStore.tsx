class AnswerStore {
    public static store(level: number, answer:string) {
        const storedAnswers: string[] = JSON.parse(localStorage.getItem('answers') ?? "[]");
        storedAnswers[level] = answer;
        localStorage.setItem('answers', JSON.stringify(storedAnswers));
    }
}

export default AnswerStore;