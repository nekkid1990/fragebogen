// @ts-ignore
import QuestionsData from "../data/QuestionData";

class KeyStore {
    public static store(keys: string, level: number) {
        const storedKeys: { [key: string]: number } = JSON.parse(localStorage.getItem('keys') ?? "{}");
        const newKeys: string[] = keys.split('-');
        for (let i = 0; i < newKeys.length; i++) {
            if (storedKeys[newKeys[i]] === undefined) {
                storedKeys[newKeys[i]] = 0;
            }
            storedKeys[newKeys[i]] += level;
        }
        localStorage.setItem('keys', JSON.stringify(storedKeys));
    }

    public static get(): { [key: string]: number } {
        return JSON.parse(localStorage.getItem('keys') ?? "{}");
    }

    public static getTotal(): { [key: string]: number } {
        const output: { [key: string]: number } = {};
        for (const questionData of QuestionsData) {
            const keys: string[] = questionData.key.split('-');
            for (let i = 0; i < keys.length; i++) {
                if (output[keys[i]] === undefined) {
                    output[keys[i]] = 0;
                }
                output[keys[i]] += 4;
            }
        }
        return output;
    }

    public static getByKey(key: string): number {
        return this.get()[key] ?? 0;
    }

    public static getTotalByKey(key: string): number {
        return this.getTotal()[key] ?? 0;
    }
}

export default KeyStore;