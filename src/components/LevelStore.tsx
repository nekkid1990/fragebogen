class LevelStore {
    public static store(questionIndex: number, level: number) {
        const storedLevels: number[] = JSON.parse(localStorage.getItem('levels') ?? "[]");
        storedLevels[questionIndex] = level;
        localStorage.setItem('levels', JSON.stringify(storedLevels));
    }

    static get(): number[] {
        return JSON.parse(localStorage.getItem('levels') ?? "[]");
    }

    static getByIndex(questionIndex: number): number {
        return JSON.parse(localStorage.getItem('levels') ?? "[]")[questionIndex] ?? 1;
    }
}

export default LevelStore;