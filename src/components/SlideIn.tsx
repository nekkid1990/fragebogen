import React from "react";
import "./SlideIn.css";

interface SlideInProps {
  backgroundClass?: string;
}

class SlideIn extends React.Component<SlideInProps, any> {
  private refContainer: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();

  constructor(props: SlideInProps) {
    super(props);
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.refContainer.current?.classList.add("active");
    }, 1);
  }

  render() {
    return (
      <>
        <div
          className={
            "layer"
            // +
            // (this.props.backgroundClass ? " " + this.props.backgroundClass : "")
          }
        >
          <div ref={this.refContainer}>{this.props.children}</div>
        </div>
      </>
    );
  }
}

export default SlideIn;
