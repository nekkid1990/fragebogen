export interface LevelDataType {
    heading: string,
    text: string,
}

const LevelData: LevelDataType[] = [{"heading":"Immerhin!","text":"Eine Basis steht, jetzt gilt es dran zu bleiben."},{"heading":"Nice!","text":"Schon fast professionell oder zumindest auf \u201anem guten Weg."},{"heading":"Sauber!","text":"Geht kaum besser, aber ein Luft nach oben ist da."},{"heading":"Top!","text":"Sieht doch wirklich gut aus!"}];

export default LevelData;