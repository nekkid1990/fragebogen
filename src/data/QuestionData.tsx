import sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS from './pictures/excel.png'

export interface QuestionDataType {
    question: string,
    technology: string,
    key: string,
    img: string,
}

const QuestionsData: QuestionDataType[] = [{
                'question': 'Nutzt du Excel?',
                'technology': 'Excel',
                'key': 'm-c-t-T',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            },{
                'question': 'Nutzt du Word?',
                'technology': 'Word',
                'key': 'm-c-t-e-w-T',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            },{
                'question': 'Nutzt du WordPress?',
                'technology': 'WordPress',
                'key': 'm-c-w-e-b-W',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            },{
                'question': 'Nutzt du Pages?',
                'technology': 'Pages',
                'key': '',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            },{
                'question': 'Nutzt du Facebook Pages?',
                'technology': 'Facebook Pages',
                'key': 'm-c-w-e-a-b-W-A',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            },{
                'question': 'Nutzt du Facebook Ads?',
                'technology': 'Facebook Ads',
                'key': 'm-c-t-e-w-a-b-T-A',
                'img': sYDzITRSiLxOmWOzMzWpAtGCrnFmHeMHifNxJpHnGyoXCiWzCkWaeZuVDmxbMDJBNZgvAZjgngItzKPcIVkKHRjtmshOChRTGMxS
            }];

export default QuestionsData;