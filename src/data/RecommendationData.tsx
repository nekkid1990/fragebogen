export interface RecommendationDataType {
    active: string[],
    passive: string[]
}

const RecommendationData: RecommendationDataType = {
    active: ["Schau dir mal folgendes an \u2026","Das ist auch interessant \u2026","Schau dir mal folgendes an \u2026","Das ist auch interessant \u2026","Schau dir mal folgendes an \u2026","Das ist auch interessant \u2026"],
    passive: ["Hier wird dir geholfen.","Gut zu wissen \u2026","Agentur macht das.","Hier wird dir geholfen.","Oder mal hier versuche \u2026","Geheimtipp:"]
};

export default RecommendationData;