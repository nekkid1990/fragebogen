import React from "react";
import "./LevelSelection.scss";
import Question from "./Question";
import LevelData, { LevelDataType } from "../data/Meta";
import LevelStore from "../components/LevelStore";

interface LevelSelectionProps {
  question: Question;
}

class LevelSelection extends React.Component<LevelSelectionProps, any> {
  private oldLevel: number = 1;

  private refBlockOne: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();
  private refBlockTwo: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();
  private refBlockThree: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();
  private refBlockFour: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();

  constructor(props: LevelSelectionProps) {
    super(props);
  }

  componentDidMount(): void {
    this.colorise();
    this.oldLevel = this.loadLevel();
  }

  render() {
    return (
      <>
        <div className="level-selection container centered">
          <div className="header">
            <div className="grid">
              <div className="col-desk-3 col-tab-6 col-mob-6 v-center">
                <h3>Level</h3>
              </div>
              <div className="col-desk-3 col-tab-6 col-mob-6 txt-right">
                <button
                  type="button"
                  className="close"
                  aria-label="Close"
                  onClick={() => {
                    this.saveLevel(this.oldLevel);
                    this.props.question.setState({
                      isLevelSelectionShown: false
                    });
                  }}
                ></button>
              </div>
            </div>
          </div>
          <div className="content">
            <div className="grid">
              {this.renderLevel()}

              <div
                className="level-selector grid"
                onTouchStart={this.touchStart}
                onTouchMove={this.touchMove}
              >
                <div ref={this.refBlockOne}>★</div>
                <div ref={this.refBlockTwo}>★</div>
                <div ref={this.refBlockThree}>★</div>
                <div ref={this.refBlockFour}>★</div>
              </div>
            </div>
            <div className="buttons grid">
              <button
                type="button"
                className="primary"
                onClick={() => {
                  this.props.question.setState({
                    currentLevel: this.loadLevel(),
                    isLevelSelectionShown: false
                  });
                }}
              >
                speichern
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }

  private renderLevel() {
    const currentLevel: number = this.loadLevel();
    const levelData: LevelDataType = LevelData[currentLevel - 1];
    return (
      <>
        <div className="col-desk12 col-tab-12 col-mob-12 v-h-center">
          <h1>{currentLevel}</h1>
        </div>
        <div className="col-desk12 col-tab-12 col-mob-12 v-h-center">
          <h2>{levelData.heading}</h2>
        </div>
        <div className="col-desk12 col-tab-12 col-mob-12 v-h-center">
          <h3>{levelData.text}</h3>
        </div>
      </>
    );
  }

  private touchStart = (e: React.TouchEvent<HTMLDivElement>) => {
    this.calcPos(e);
  };

  private touchMove = (e: React.TouchEvent<HTMLDivElement>) => {
    this.calcPos(e);
  };

  private calcPos(e: React.TouchEvent<HTMLDivElement>) {
    const rect: any = e.currentTarget.getBoundingClientRect();
    const posX: number =
      -1 * (rect.left + window.pageXOffset - e.touches[0].clientX);
    const posY: number = rect.top + window.pageYOffset - e.touches[0].clientY;
    const width: number = e.currentTarget.clientWidth;
    const level: number = posX / (width / 4);
    this.saveLevel(Math.ceil(level));
    this.colorise();
    this.forceUpdate();
  }

  private colorise() {
    const level: number = this.loadLevel();
    if (level > 3) {
      this.refBlockOne.current?.classList.add("selected");
      this.refBlockTwo.current?.classList.add("selected");
      this.refBlockThree.current?.classList.add("selected");
      this.refBlockFour.current?.classList.add("selected");
    } else if (level > 2) {
      this.refBlockOne.current?.classList.add("selected");
      this.refBlockTwo.current?.classList.add("selected");
      this.refBlockThree.current?.classList.add("selected");
      this.refBlockFour.current?.classList.remove("selected");
    } else if (level > 1) {
      this.refBlockOne.current?.classList.add("selected");
      this.refBlockTwo.current?.classList.add("selected");
      this.refBlockThree.current?.classList.remove("selected");
      this.refBlockFour.current?.classList.remove("selected");
    } else if (level > 0) {
      this.refBlockOne.current?.classList.add("selected");
      this.refBlockTwo.current?.classList.remove("selected");
      this.refBlockThree.current?.classList.remove("selected");
      this.refBlockFour.current?.classList.remove("selected");
    }
  }

  private saveLevel(level: number) {
    const adjustedLevel: number = Math.min(Math.max(level, 1), 4);
    LevelStore.store(this.props.question.props.current - 1, adjustedLevel);
  }

  private loadLevel(): number {
    return LevelStore.getByIndex(this.props.question.props.current - 1);
  }
}

export default LevelSelection;
