import React from "react";
import Questionnaire from "./Questionnaire";
import "./Question.scss";
import { QuestionDataType } from "../data/QuestionData";
import SlideIn from "../components/SlideIn";
import LevelSelection from "./LevelSelection";
import KeyStore from "../components/KeyStore";
import AnswerStore from "../components/AnswerStore";
import LevelStore from "../components/LevelStore";

interface QuestionProps {
  current: number;
  end: number;
  questionnaire: Questionnaire;
  data: QuestionDataType;
}

class Question extends React.Component<QuestionProps, any> {
  constructor(props: QuestionProps) {
    super(props);
    this.state = {
      currentLevel: 1,
      isLevelSelectionShown: false
    };
  }

  renderSwitch(param: number) {
    switch (param) {
      case 1:
        return "★";
      case 2:
        return "★ ★";
      case 3:
        return "★ ★ ★";
      case 4:
        return "★ ★ ★ ★";
      default:
        return "Bewerten";
    }
  }

  render() {
    return (
      <>
        <div className="question container centered">
          <div className="header">
            <div className="grid">
              <div className="col-desk-3 col-tab-6 col-mob-6">
                <h3>Frage</h3>
              </div>
              <div className="col-desk-3 col-tab-6 col-mob-6 txt-right">
                <h3>{this.props.current + "/" + this.props.end}</h3>
              </div>
            </div>
          </div>
          <div className="content">
            <div className="image grid">
              <div className="col-desk12 col-tab-12 col-mob-12">
                <img alt="" src={this.props.data.img} />
              </div>
            </div>
            <div className="title grid">
              <div className="col-desk12 col-tab-12 col-mob-12">
                <h2>{this.props.data.question}</h2>
              </div>
            </div>

            {/* levels */}
            <div className="grid level">
              <div className="col-desk12 col-tab-12 col-mob-12">
                <div className="inner-container" onClick={this.clickLevel}>
                  {/* {this.state.currentLevel
                    ? this.state.currentLevel + "/4"
                    : "level"} */}
                  <button className="">
                    {this.renderSwitch(this.state.currentLevel)}
                  </button>
                </div>
              </div>
            </div>

            {/* buttons horizontal */}

            <div className="buttons horizontal grid">
              <div className="col-desk3 col-tab-6 col-mob-6">
                <button type="button" className="" onClick={this.clickYes}>
                  Ja
                </button>
              </div>
              <div className="col-desk3 col-tab-6 col-mob-6">
                <button
                  type="button"
                  className="primary"
                  onClick={this.clickNo}
                >
                  Nein
                </button>
              </div>
            </div>

            {/* buttons vertikal */}
            {/* <div className="buttons vertical grid">
              <div className="col-desk12 col-tab-12 col-mob-12">
                <button
                  type="button"
                  className="custome"
                  onClick={this.clickYes}
                >
                  Ja
                </button>
              </div>
              <div className="col-desk12 col-tab-12 col-mob-12">
                <button
                  type="button"
                  className="primary"
                  onClick={this.clickNo}
                >
                  Nein
                </button>
              </div>
            </div> */}

            {/* skip */}
            <div className="skip">
              <div className="grid">
                <div className="col-desk12 col-tab-12 col-mob-12">
                  <button
                    type="button"
                    className="light"
                    onClick={this.clickSkip}
                  >
                    überspringen
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.renderLevelSelection()}
      </>
    );
  }

  private renderLevelSelection() {
    if (this.state.isLevelSelectionShown) {
      return (
        <SlideIn backgroundClass="Question-level-selection-bg">
          <LevelSelection question={this} />
        </SlideIn>
      );
    }
    return null;
  }

  private clickLevel = (e: React.MouseEvent<HTMLDivElement>) => {
    this.setState({
      isLevelSelectionShown: true
    });
  };

  private clickYes = (e: React.MouseEvent<HTMLButtonElement>) => {
    LevelStore.store(this.props.current - 1, this.state.currentLevel);
    AnswerStore.store(this.props.current - 1, "yes");
    KeyStore.store(this.props.data.key, this.state.currentLevel);
    this.props.questionnaire.nextQuestion();
  };

  private clickNo = (e: React.MouseEvent<HTMLButtonElement>) => {
    LevelStore.store(this.props.current - 1, 0);
    AnswerStore.store(this.props.current - 1, "no");
    this.props.questionnaire.nextQuestion();
  };

  private clickSkip = (e: React.MouseEvent<HTMLButtonElement>) => {
    LevelStore.store(this.props.current - 1, 0);
    AnswerStore.store(this.props.current - 1, "none");
    this.props.questionnaire.nextQuestion();
  };
}

export default Question;
