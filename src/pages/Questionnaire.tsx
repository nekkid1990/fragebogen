import React from "react";
import QuestionsData, { QuestionDataType } from "../data/QuestionData";
import Question from "./Question";
import SlideIn from "../components/SlideIn";
import "./Questionnaire.scss";

class Questionnaire extends React.Component<any, any> {
  private currentQuestionIndex: number = 0;

  constructor(props: any) {
    super(props);
    localStorage.clear();
  }

  render() {
    return <>{this.getCurrentQuestion()}</>;
  }

  private getCurrentQuestion() {
    const questionData: QuestionDataType =
      QuestionsData[this.currentQuestionIndex];
    if (questionData !== undefined) {
      return (
        <SlideIn key={this.currentQuestionIndex} backgroundClass="container">
          <Question
            key="currentQuestion"
            questionnaire={this}
            current={this.currentQuestionIndex + 1}
            end={QuestionsData.length}
            data={questionData}
          />
        </SlideIn>
      );
    }
    return null;
  }

  public nextQuestion() {
    if (this.currentQuestionIndex < QuestionsData.length - 1) {
      this.currentQuestionIndex++;
    } else {
      // @ts-ignore
      window.location = "/result";
    }

    this.forceUpdate();
  }
}

export default Questionnaire;
