import React from "react";
import "./Results.scss";
import KeyStore from "../components/KeyStore";
import QuestionsData from "../data/QuestionData";
import LevelStore from "../components/LevelStore";
import RecommendationData from "../data/RecommendationData";
import SkillData from "../data/SkillData";
import LevelData from "../data/Meta";
import CountUp from "react-countup";

class Results extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const percentage: number = this.calculatePercentage();
    let heading: string = "";
    let label: string = "";
    if (percentage >= 0 && percentage < 25) {
      heading = LevelData[0].heading;
      label = LevelData[0].text;
    } else if (percentage >= 25 && percentage < 50) {
      heading = LevelData[1].heading;
      label = LevelData[1].text;
    } else if (percentage >= 50 && percentage < 75) {
      heading = LevelData[2].heading;
      label = LevelData[2].text;
    } else if (percentage >= 75 && percentage <= 100) {
      heading = LevelData[3].heading;
      label = LevelData[3].text;
    }
    return (
      <>
        <div className="results container">
          <div className="header">
            <div className="grid">
              <div className="col-desk-12 col-tab-12 col-mob-12">
                <h3>Status</h3>
              </div>
            </div>
          </div>
          <div className="content">
            <div className="grid">
              <div className="col-desk12 col-tab-12 col-mob-12 result-list">
                <div className="score">
                  <h1 className="score-value">
                    <CountUp start={0} end={percentage} delay={0} duration={6}>
                      {({ countUpRef }) => (
                        <div>
                          <span ref={countUpRef} />
                          <span>%</span>
                        </div>
                      )}
                    </CountUp>
                  </h1>
                  <h2>{heading}</h2>
                  <h3>{label}</h3>
                </div>
                {this.renderTechnologies()}
                {this.renderTechnologiesDetails()}
                {this.renderSkills()}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  private calculatePercentage(): number {
    const totalKeys: { [key: string]: number } = KeyStore.getTotal();
    const storedKeys: { [key: string]: number } = KeyStore.get();
    const percentageKeys: number[] = [];
    for (const key of Object.keys(totalKeys)) {
      if (storedKeys[key] === undefined) {
        percentageKeys.push(0.0);
      } else {
        percentageKeys.push(storedKeys[key] / totalKeys[key]);
      }
    }
    let totalPercentage: number = 0.0;
    for (let i = 0; i < percentageKeys.length; i++) {
      totalPercentage += percentageKeys[i];
    }
    return Math.round((totalPercentage / percentageKeys.length) * 100.0);
  }

  private renderTechnologies() {
    const output: any[] = [];
    let i: number = 0;
    const levels: string[] = [
      " bar_0",
      " bar_1",
      " bar_2",
      " bar_3",
      " bar_4",
      " bar_5"
    ];
    for (const questionData of QuestionsData) {
      const percentage: number = Math.round(
        (LevelStore.getByIndex(i) / 4) * 100
      );
      output.push(
        <div
          key={questionData.technology}
          className={"technology bar-wrap" + (i === 5 ? " last" : "")}
        >
          <div
            className={"bar" + levels[i % 6]}
            style={{ width: percentage + "%" }}
          ></div>
          <div className="label">{questionData.technology}</div>
        </div>
      );
      i++;
    }
    return output;
  }

  private renderTechnologiesDetails() {
    const output: any[] = [];
    let i: number = 0;
    for (const questionData of QuestionsData) {
      const percentage: number = Math.round(
        (LevelStore.getByIndex(i) / 4) * 100
      );
      let activeHint = null;
      if (percentage <= 50) {
        activeHint = (
          <div className="hint paragraph">{RecommendationData.active[i]}</div>
        );
      }
      output.push(
        <div key={questionData.technology} className="details">
          <h2>{questionData.technology}</h2>
          <div className={activeHint ? "hint paragraph" : "hint paragraph dn"}>
            {activeHint}
          </div>
          <div className="advice paragraph">
            {RecommendationData.passive[i]}
          </div>
        </div>
      );
      i++;
    }
    return output;
  }

  private renderSkills() {
    const output: any[] = [];
    let i: number = 0;
    for (const skillData of SkillData) {
      const keyLevel: number = KeyStore.getByKey(skillData.key);
      const keyTotalLevel: number = KeyStore.getTotalByKey(skillData.key);
      const percentage: number = Math.round((keyLevel / keyTotalLevel) * 100);
      let comment = null;
      if (percentage >= 75 && percentage <= 100) {
        comment = <div className="active-hint">{skillData.l4}</div>;
      } else if (percentage >= 50 && percentage < 75) {
        comment = <div className="active-hint">{skillData.l3}</div>;
      } else if (percentage >= 25 && percentage < 50) {
        comment = <div className="active-hint">{skillData.l2}</div>;
      } else if (percentage >= 0 && percentage < 25) {
        comment = <div className="active-hint">{skillData.l2}</div>;
      }
      output.push(
        <div key={skillData.skill} className="skills details">
          <h2>{skillData.skill}</h2>
          <div className="paragraph">{skillData.description}</div>
          <div className="advice paragraph">{comment}</div>
        </div>
      );
      i++;
    }
    return output;
  }
}

export default Results;
