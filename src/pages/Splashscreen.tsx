import React from "react";
import "./Splashscreen.scss";

class Splashscreen extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <>
        <div className="splash container centered test-image">
          <div className="grid trans-bg">
            <h1>h1 Headline</h1>
            <h2 className="invert">h2 Headline invert</h2>
            <h3>h3 Headline</h3>
            <p>
              copytext copytext copytext copytext copytext copytext copytext
              copytext copytext copytext copytext copytext copytext
            </p>
            {/* 
            <p>
              copytext copytext copytext copytext copytext copytext copytext
              copytext copytext copytext copytext copytext copytext
            </p>
            <div className="col-desk-3 col-tab-6 col-mob-12">
              <button
                type="button"
                className="primary"
                onClick={this.showQuestions}
              >
                primary
              </button>
            </div>
            <div className="col-desk-3 col-tab-6 col-mob-12">
              <button
                type="button"
                className="secondary"
                onClick={this.showQuestions}
              >
                secondary
              </button>
            </div>
            <div className="col-desk-3 col-tab-6 col-mob-12">
              <button
                type="button"
                className="secondary invert"
                onClick={this.showQuestions}
              >
                2nd Invert
              </button>
            </div>
            <div className="col-desk-3 col-tab-6 col-mob-12">
              <button
                type="button"
                className="custom"
                onClick={this.showQuestions}
              >
                custom
              </button>
            </div>
            <div className="col-desk-3 col-tab-6 col-mob-12">
              <button
                type="button"
                className="custom invert"
                onClick={this.showQuestions}
              >
                custom invert
              </button>
            </div>
            <div className="col-desk-12 col-tab-12 col-mob-12">
              <button
                type="button"
                className="primary"
                onClick={this.showQuestions}
              >
                Solo-Button
              </button>
            </div> */}
            <div className="col-desk-12 col-tab-12 col-mob-12">
              <button
                type="button"
                className="primary xl"
                onClick={this.showQuestions}
              >
                Solo-Button XL
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }

  private showQuestions = () => {
    // @ts-ignore
    window.location = "/questionnaire";
  };
}

export default Splashscreen;
